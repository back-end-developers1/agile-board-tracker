class Header:
    def __init__(self, sid=0, name='', active=1):
        self.sSid = str(sid)
        self.sName = name

    def getAsList(self):
        return [str(self.sSid), str(self.sName)]

    def setFromList(self, data):
        if len(data) == 2:
            self.sSid = data[0]
            self.sName = data[1]
    
    def getColumnsString(self):
        return "header_id, header_name"

    def getUpdateColumnsString(self):
        return "header_id = {}, header_name = '{}'".format(int(self.sSid), self.sName)