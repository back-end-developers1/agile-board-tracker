# represents an iteration, such as a scrum sprint

class Iteration:
    def __init__(self, sid=0, name='', start='', end=''):
        self.sSid = str(sid)
        self.sName = name
        self.sStart = start
        self.sEnd = end
    
    def setFromList(self,data):
        if len(data) == 4:    
            self.sSid = data[0]
            self.sName = data[1]
            self.sStart = data[2]
            self.sEnd = data[3]

    def getColumnsString(self):
        return "iteration_id, iteration_name, iteration_start, iteration_end"
    
    def getUpdateColumnsString(self):
        return "iteration_id = {}, iteration_name = '{}', iteration_start='{}',iteration_end='{}'".format(
            int(self.sSid), self.sName, self.sStart, self.sEnd
        )