# Represents a work item from the release
# Scrum may use all of this, whereas kanban or XP would use hardly any

class Item:
    def __init__(self, sid=0, name='', category='', status=0, epic=0, points=0, desc='', iteration='Backlog'):
        self.sSid = str(sid)
        self.sName = name
        self.sCategory = category
        self.sStatus = str(status)
        self.sEpic = str(epic)
        self.sPoints = str(points)
        self.sDesc = desc
        self.sIteration = iteration

    def setFromList(self, data=[]):
        if len(data) == 7:
            self.sSid = data[0]
            self.sName = data[1]
            self.sCategory = data[2]
            self.sStatus = data[3]
            self.sEpic = data[4]
            self.sPoints = data[5]
            self.sDesc = data[6]

    def getAsList(self):
        # this should be equal size to the columns
        # use getColumns as reference
        return [
            self.sSid,
            self.sIteration,
            self.sName,
            self.sCategory,
            self.sStatus,
            self.sEpic,
            self.sPoints,
            self.sDesc]

    def getColumnsString(self):
        return "item_id, item_name, item_category, header_name, epic_name, item_points, item_description"
    
    def getUpdateColumnsString(self):
        return """item_name = '{}', item_category = '{}', header_name = '{}', epic_name = '{}', item_points = {}, item_description = '{}'
        """.format(self.sName,self.sCategory,self.sStatus,self.sEpic,int(self.sPoints),self.sDesc)      

    def getColumns(self):
        # returns a dict of column headers
        # if item class changes, this will guarantee
        # that the gui still works properly
        # key is used as a column index
        # value is list:
        #   element 1: column name
        #   element 2: column width
        return {
            0 : ['Id',30],
            1 : ['Iteration',70],
            2 : ['Name',120],
            3 : ['Type',80],
            4 : ['Status',80],
            5 : ['Epic',100],
            6 : ['Points',50],
            7 : ['Description',250]
            }