# placeholder for the epic class

class Epic:
    def __init__(self, sid=0, name=''):
        self.sSid = str(sid)
        self.sName = name
    
    def setFromList(self,data=[]):
        if len(data) == 2:
            self.sSid = data[0]
            self.sName = data[1]
    
    def getColumnsString(self):
        return "epic_id, epic_name"
    
    def getUpdateColumnsString(self):
        return "epic_id = {}, epic_name = '{}'".format(int(self.sSid),self.sName)