# placeholder for the transaction class

class Transaction:
    def __init__(self, sid=0,oldHeader='',newHeader='',iterationItem=0,date=''):
        self.sSid = str(sid)
        self.sOld = oldHeader
        self.sNew = newHeader
        self.nIterationItem = iterationItem
        self.sDate = date
    
    def setFromList(self,data):
        if len(data) == 5:
            self.sSid = data[0]
            self.sOld = data[1]
            self.sNew = data[2]
            self.nIterationItem = data[3]
            self.sDate = data[4]

    def getAsList(self):
        return [
            self.sSid,
            self.sOld,
            self.sNew,
            self.nIterationItem,
            self.sDate           
        ]

    def getColumnsString(self):
        return "transaction_id, transaction_oldHeader, transaction_newHeader, iterationItem_id, transaction_date"
    
    def getUpdateColumnsString(self): # 5
        return """transaction_oldHeader = '{}', transaction_newHeader = '{}', iterationItem_id = {}, transaction_date = '{}'
        """.format(self.sOld,self.sNew,self.nIterationItem,self.sDate)