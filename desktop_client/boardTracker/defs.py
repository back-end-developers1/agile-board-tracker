import os, platform, sqlite3

from obj.item import Item
from obj.header import Header
from obj.iteration import Iteration
from obj.transaction import Transaction
from obj.epic import Epic

# provides a central place for definitions and global functions

class Def:
    def __init__(self):
        pass

    # list controls
    LC_ITERATIONS = 0
    LC_ITEMS = 1

    # Constants
    ALL = '*ALL*'

    # table
    TABLE_ITEM = 0
    TABLE_HEADER = 1
    TABLE_TRANSACTION = 2
    TABLE_ITERATION = 3
    TABLE_EPIC = 4
    TABLE_ITERATIONITEM = 5

    #  titles
    TITLE_DESKTOP = 'Agile Board Tracker'
    TITLE_ADD = 'Add Frame'
    TITLE_SERVER = 'Server'
    TITLE_HEADER = 'Header Management'
    TITLE_ITERATION = 'Iteration Management'
    TITLE_ITERATIONS = 'Iterations'
    TITLE_ITEMS = 'Items'

    # file menus
    MENU_FILE = '&File'
    MENU_SERVER = '&Server'
    MENU_HEADER = '&Header'
    MENU_ITERATION = '&Iteration'
    MENU_PRINT = '&Print'
    MENU_HELP = 'H&elp'

    # file menu options
    FILE_QUIT = 'Quit'
    FILE_OPEN = 'Open'
    FILE_MANAGE = 'Manage'
    FILE_PRINT = 'Print'
    FILE_ITEMS = 'Items'
    FILE_HEADERS = 'Headers'
    FILE_BURN_SPRINT = 'Sprint Burndown'
    FILE_BURN_RELEASE = 'Release Burndown'
    FILE_SERVER = 'Server'
    FILE_PRINTING = 'Printing'
    FILE_MANUAL = 'User Manual'
    FILE_ABOUT = 'About'

    # buttons
    BUTTON_ADD_ENTRY = 'Add Entry'
    BUTTON_DELETE_ENTRY = 'Delete Entry'
    BUTTON_ADD = 'Add'
    BUTTON_CANCEL = 'Cancel'
    BUTTON_START = 'Start'
    BUTTON_UPDATE = 'Update'
    BUTTON_DELETE = 'Delete'
    BUTTON_SAVE = 'Save'

    # labels
    LABEL_NAME = 'Name'
    LABEL_TYPE = 'Type'
    LABEL_STATUS = 'Status'
    LABEL_EPIC = 'Epic'
    LABEL_POINTS = 'Points'
    LABEL_DESC = 'Description'
    LABEL_ITERATION = 'Iterations'
    LABEL_BACKLOG = 'Backlog'

    # info text
    INFO_SERVER = ('This is the server menu. After capturing an image from the phone app, '
    'start the server with the button below, and start the client on the phone app.\n'
    'Once data transfer is complete, the server and client will close automatically.\n'
    'Pressing cancel will also stop the server.'
    '\nThe IP address to connect to is: ')
    INFO_HEADER = ('Check the boxes to set it to be used or not.')

    def getRootDir(self=None):
        slash = '\\'
        PLATFORM = platform.system().lower()
        if (PLATFORM == 'linux') or (PLATFORM == 'darwin'):
            slash = '/'
        return os.getcwd()+slash

    def getTableString(self=None, table=None, sid=False, name=False):
        """
        Gets the table name as a string
        If id is set to True, it appends _id to the string
        """
        t = False
        if table == Def.TABLE_HEADER:
            t = 'header'
        elif table == Def.TABLE_ITEM:
            t = 'item'
        elif table == Def.TABLE_ITERATION:
            t = 'iteration'
        elif table == Def.TABLE_TRANSACTION:
            t = 'transaction'
        elif table == Def.TABLE_EPIC:
            t = 'epic'
        elif table == Def.TABLE_ITERATIONITEM:
            t = 'iterationItem'
        
        if sid:
            t = t + '_id'
        elif name:
            t = t + '_name'
        
        if table == Def.TABLE_TRANSACTION:
            t = '\"' + t + '\"'

        return t

    def getTableObject(self=None, table=None):
        if table == Def.TABLE_HEADER:
            obj = Header()
        elif table == Def.TABLE_ITEM:
            obj = Item()
        elif table == Def.TABLE_ITERATION:
            obj = Iteration()
        elif table == Def.TABLE_TRANSACTION:
            obj = Transaction()
        elif table == Def.TABLE_EPIC:
            obj = Epic()
        elif table == Def.TABLE_ITERATIONITEM:
            obj = IterationItem()
        else:
            obj = False
        return obj

    def isMatching(list1=[], list2=[]):
        # get true or false if matching
        return sorted(list1) == sorted(list2)
    
    def runSQLScript(dbName='', scriptName=None):
        conn = sqlite3.connect(dbName + '.db')

        with open(scriptName, 'r') as s:
            sqlScript = s.read()
            conn.executescript(sqlScript)
        s.closed

        conn.close()
