import random as r

from view.gui import Gui
from defs import Def
from obj.item import Item
from obj.header import Header
from obj.iteration import Iteration
from obj.transaction import Transaction
from obj.epic import Epic
from testing.test import Test

import os

def main():
    # for running the gui, aka the actual application
    ui = Gui()

    # for running tests. Match the Test.Constant to a constant defined in the test.py file to run the test
    # or leave it empty to run all tests
    #test = Test.run(Test.DATABASE_GET)
    #test = Test.run()

if __name__ == "__main__":
    main()
