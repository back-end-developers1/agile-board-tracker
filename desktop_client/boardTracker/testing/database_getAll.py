# Agile Board Tracker
# Author: Joseph Holland
# Test Scenario: Database Operation
# Test Case: GetAll Function

from defs import Def
from obj.header import Header
from db.dbHandler import DBHandler

class Test_database_getAll:

    # Preconditions
    # - db browser is installed
    # - database creation test case has passed

    def run(self, db):

        # 3. Ensure the DB has 2 rows
        # first row:
        #   id = 0
        #   name = New
        # second row:
        #   id = 1
        #   name = User Story

        headersToTest = db.getAll(Def.TABLE_HEADER)
        testData = [Header(0,'New'), Header(1,'User Story')]

        if len(headersToTest) != len(testData):
            print('Length of recieved headers is different from test data')
            for h in headersToTest:
                print(h.getAsList())
            p = [False]
        else:
            p = []
            for i in range(len(headersToTest)):
                # get true or false if matching
                headerToTest = headersToTest[i].getAsList()
                td = testData[i].getAsList()
                result = Def.isMatching(td, headerToTest)
                print('Header to Test:', headerToTest)
                print('Test Data:', td)
                print('Pass?', result)
                p.append(result)

        # print output
        r = True
        for b in p:
            r = r & b
        return r