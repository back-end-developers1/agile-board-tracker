INSERT OR IGNORE INTO header (header_id, header_name)
VALUES (0, 'New');
UPDATE header SET header_name = 'New' WHERE header_id = 0;

INSERT OR IGNORE INTO header (header_id, header_name)
VALUES (1, 'User Story');
UPDATE header SET header_name = 'User Story' WHERE header_id = 1;