# Agile Board Tracker
# Author: Joseph Holland
# Test Scenario: Database Operation
# Test Case: isNameInTable Function

from defs import Def
from obj.header import Header
from db.dbHandler import DBHandler

class Test_database_isNameInTable:

    # Preconditions
    # - db browser is installed
    # - database creation test case has passed

    def run(self, db):

        # Use the header table
        # The database automatically creates a row in header if the header table is empty
        # Header id to get is 0
        
        testData = 'User Story'
        print('Name to find:', testData)
        print('Table to search in: Header')

        p = db.isNameInTable(Def.TABLE_HEADER, testData)
        print('Expected Result: True')
        print('Actual Result:', p)

        return p

