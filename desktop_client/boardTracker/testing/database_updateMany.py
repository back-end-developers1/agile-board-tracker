# Agile Board Tracker
# Author: Joseph Holland
# Test Scenario: Database Operation
# Test Case: updateMany Function

from defs import Def
from obj.header import Header
from db.dbHandler import DBHandler

class Test_database_updateMany:

    # Preconditions
    # - db browser is installed
    # - database creation test case has passed

    def run(self, db):

        # Use the header table
        # The database automatically creates a row in header if the header table is empty
        # Header id to get is 0

        originalData = [Header(0, 'New'),Header(1, 'User Story')]
        testData = [Header(0, 'In Progress'),Header(1, 'Closed')]
        for i in range(2):
            print('Original data:', originalData[i].getAsList())
            print('Data to change to:', testData[i].getAsList())

        print('Updating...')
        db.updateMany(Def.TABLE_HEADER, testData)

        results = []
        print('Getting test data...')
        for i in range(2):
            result = db.get(Def.TABLE_HEADER, i)
            if result:
                r = Def.isMatching(result.getAsList(), testData[i].getAsList())
            else:
                r = False
            print('Result:', result.getAsList())
            print('Pass?', r)
            results.append(r)

        p = True
        for r in results:
            p = r & p

        return p