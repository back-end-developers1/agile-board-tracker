# Agile Board Tracker
# Author: Joseph Holland
# Test Scenario: Database Operation
# Test Case: InsertMany Function

from defs import Def
from obj.header import Header
from db.dbHandler import DBHandler

class Test_database_insertMany:

    # Preconditions
    # - db browser is installed
    # - database creation test case has passed

    def run(self, db):

        # Use the header table
        testData = [Header(2,'In Progress'), Header(3,'Ready for Test')]
        print('Data to Insert:')
        for t in testData:
            print(t.getAsList())

        for t in testData:
            print('inserting', t.getAsList())
            db.insert(Def.TABLE_HEADER, t)

        p = True
        print('testing existence of data...')
        for t in testData:
            exists = db.get(Def.TABLE_HEADER, t.sSid)
            if exists:
                e = Def.isMatching(t.getAsList(), exists.getAsList())
            if e:
                msg = str(e) + ': ' + str(exists.getAsList())
            else:
                msg = e
                p = False
            print('Header inserted:', msg)

        return p
