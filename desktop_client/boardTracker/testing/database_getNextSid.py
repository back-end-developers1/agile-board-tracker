# Agile Board Tracker
# Author: Joseph Holland
# Test Scenario: Database Operation
# Test Case: GetNextSid Function

from defs import Def
from obj.header import Header
from db.dbHandler import DBHandler

class Test_database_getNextSid:

    # Preconditions
    # - db browser is installed
    # - database creation test case has passed

    def run(self, db):

        # use header table
        nextSidToTest = db.getNextSid(Def.TABLE_HEADER)
        nextActualSid = 2

        print('Next Actual ID:', nextActualSid)
        print('Next ID from Function:', nextSidToTest)

        r = nextSidToTest == nextActualSid
        print('Pass:', r)

        return r