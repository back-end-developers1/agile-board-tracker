# Agile Board Tracker
# Author: Joseph Holland
# Description: A module for running tests.
#              To add a test, specify an id constant
#              and add the corresponding elif and object to the getTest function

import os

from db.dbHandler import DBHandler
from defs import Def

from .database_databaseCreation import Test_database_creation
from .database_get import Test_database_get
from .database_getAll import Test_database_getAll
from .database_getName import Test_database_getName
from .database_getNextSid import Test_database_getNextSid
from .database_delete import Test_database_delete
from .database_deleteMany import Test_database_deleteMany
from .database_insert import Test_database_insert
from .database_insertMany import Test_database_insertMany
from .database_isNameInTable import Test_database_isNameInTable
from .database_update import Test_database_update
from .database_updateMany import Test_database_updateMany

class Test:

    # id constants
    # these correspond to the position in the list below, with the exception of all
    ALL = -1
    DATABASE_CREATION = 0
    DATABASE_GET = 1
    DATABASE_GETALL = 2
    DATABASE_GETNAME = 3
    DATABASE_GETNEXTSID = 4
    DATABASE_DELETE = 5
    DATABASE_DELETEMANY = 6
    DATABASE_INSERT = 7
    DATABASE_INSERTMANY = 8
    DATABASE_ISNAMEINTABLE = 9
    DATABASE_UPDATE = 10
    DATABASE_UPDATEMANY = 11

    TESTDB = os.path.join('testing', 'testDB')

    # Dictionary of tests
    # contains { id : [name, test object] }
    tests = {
        DATABASE_CREATION : ['Database Creation',Test_database_creation()],
        DATABASE_GET : ['Database get Function', Test_database_get()],
        DATABASE_GETALL : ['Database getAll Function', Test_database_getAll()],
        DATABASE_GETNAME : ['Database getName Function', Test_database_getName()],
        DATABASE_GETNEXTSID : ['Database getNextSid Function', Test_database_getNextSid()],
        DATABASE_DELETE : ['Database delete Function', Test_database_delete()],
        DATABASE_DELETEMANY : ['Database deleteMany Function', Test_database_deleteMany()],
        DATABASE_INSERT : ['Database insert Function', Test_database_insert()],
        DATABASE_INSERTMANY : ['Database insertMany Function', Test_database_insertMany()],
        DATABASE_ISNAMEINTABLE : ['Database isNameInTable Function', Test_database_isNameInTable()],
        DATABASE_UPDATE : ['Database update Function', Test_database_update()],
        DATABASE_UPDATEMANY : ['Database updateMany Function', Test_database_updateMany()]
    }

    def __init__(self):
        pass
    
    def run(testId=-1):
        separator = 100
        pSeparator = 30

        results={}

        tests = []
        if testId == Test.ALL:
            tests = [*Test.tests.values()]
        else:
            tests = [Test.tests[testId]]

        for t in tests:
            Test.cleanTestDB()
            Test.setupTestDB()

            print('='*separator)
            print('Running',t[0],'Test...\n')
            result = t[1].run(db = DBHandler(Test.TESTDB))
            results[t[0]] = result

            print()
            print('*'*pSeparator)
            print('PASS:', result)
            print('*'*pSeparator)
            
            print('\nCleaning up test area...')
            Test.cleanTestDB()

            print('Test Finished')
            print('='*separator)
        
        Test.printResults(results)

    def printResults(results):
        print('='*20, 'RESULTS', '='*71)
        passes = 0
        totalValid = 0
        for k,v in results.items():
            print(k + ':',v)
            if isinstance(v, (int)):
                totalValid += 1
                if v:
                    passes += 1
        print('~'*50)
        print('Total Automated:',totalValid)
        print('Passes:', passes)
        print('Fails:', totalValid - passes)
        print('Pass rate:', str(passes / totalValid * 100) + '%')
        print('~'*50)

    def setupTestDB():
        print('Setting up Test Database...')
        db = DBHandler(Test.TESTDB)
        sqlName = os.path.join('testing','test.sql')
        Def.runSQLScript(Test.TESTDB, sqlName)

    def cleanTestDB():
        if os.path.exists(Test.TESTDB+'.db'):
            os.remove(Test.TESTDB+'.db')       