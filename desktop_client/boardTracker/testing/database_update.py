# Agile Board Tracker
# Author: Joseph Holland
# Test Scenario: Database Operation
# Test Case: update Function

from defs import Def
from obj.header import Header
from db.dbHandler import DBHandler

class Test_database_update:

    # Preconditions
    # - db browser is installed
    # - database creation test case has passed

    def run(self, db):

        # Use the header table
        # The database automatically creates a row in header if the header table is empty
        # Header id to get is 0

        originalData = Header(1, 'User Story')
        testData = Header(1, 'Closed')

        print('Original data:', originalData.getAsList())
        print('Data to change to:', testData.getAsList())

        print('Updating...')
        db.update(Def.TABLE_HEADER, testData)

        print('Getting test data...')
        result = db.get(Def.TABLE_HEADER, 1)
        if result:
            p = Def.isMatching(result.getAsList(), testData.getAsList())
        else:
            p = False
        print('Result:', result.getAsList())
        print('Pass?', p)

        return p