# Agile Board Tracker
# Author: Joseph Holland
# Test Scenario: Database Operation
# Test Case: Delete Function

import os

from defs import Def
from obj.header import Header
from db.dbHandler import DBHandler

class Test_database_delete:

    # Preconditions
    # - db browser is installed
    # - database creation test case has passed

    def run(self, db):

        # Use the header table
        testData = Header(0,'New')
        print('Header to Delete:', testData.getAsList())

        db.delete(Def.TABLE_HEADER, 0)

        exists = db.get(Def.TABLE_HEADER, 0)
        p = False if exists else True
        print('Header Still Exists:', p)

        return p

