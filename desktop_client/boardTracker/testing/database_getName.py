# Agile Board Tracker
# Author: Joseph Holland
# Test Scenario: Database Operation
# Test Case: GetName Function

import os

from defs import Def
from obj.header import Header
from db.dbHandler import DBHandler

class Test_database_getName:

    # Preconditions
    # - db browser is installed
    # - database creation test case has passed

    def run(self, db):

        # Use the header table
        # The database automatically creates a row in header if the header table is empty
        # Header id to get is 0
        headerToTest = db.getByName(Def.TABLE_HEADER, 'New')
        testData = Header(0,'New')

        testDataList = testData.getAsList()
        headerToTestList = headerToTest.getAsList()

        # get true or false if matching
        p = Def.isMatching(testDataList, headerToTestList)

        # print output
        print('Test Data:', testDataList)
        print('From database:', headerToTestList)
        return p

