# Agile Board Tracker
# Author: Joseph Holland
# Test Scenario: Database Operation
# Test Case: DeleteMany Function

import os

from defs import Def
from obj.header import Header
from db.dbHandler import DBHandler

class Test_database_deleteMany:

    # Preconditions
    # - db browser is installed
    # - database creation test case has passed

    def run(self, db):

        # Use the header table
        testData = [Header(0,'New'), Header(1,'User Story')]
        print('Headers to Delete:')
        for t in testData:
            print(t.getAsList())

        db.deleteMany(Def.TABLE_HEADER, [0,1])

        exists = db.getAll(Def.TABLE_HEADER)
        p = False if exists else True
        print('Headers Still Exist:', p)

        return p

