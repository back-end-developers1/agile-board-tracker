# Agile Board Tracker
# Author: Joseph Holland
# Test Scenario: Database Operation
# Test Case: Insert Function

from defs import Def
from obj.header import Header
from db.dbHandler import DBHandler

class Test_database_insert:

    # Preconditions
    # - db browser is installed
    # - database creation test case has passed

    def run(self, db):

        # Use the header table
        testData = Header(2,'In Progress')

        print('Data to Insert:', testData.getAsList())

        db.insert(Def.TABLE_HEADER, testData)

        # print output
        exists = db.get(Def.TABLE_HEADER, 2)

        p = True if exists else False
        if p:
            msg = exists.getAsList()
        else:
            msg = p
        print('Header from Table:', msg)

        return p
