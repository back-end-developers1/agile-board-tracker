Author: Joseph Holland

boardTracker

Prerequisites;
1. Ensure dependencies are met, run;
	pip3 install -r requirements.txt
	install wxpython, this depends on your OS and python version
            - Windows and Mac, use pip install -U wxPython
            - Linux, download wheel from https://extras.wxpython.org/wxPython4/extras/linux/
                - I used gtk3/ubuntu-18.04/wxPython-4.0.7.post2-cp36-cp36m-linux_x86_64.whl
                - cpXX corresponds with the python version
                - run pip3 install --user [wheel name]

Installation;
1. Currently none. It is run from the boardTracker directory.

Running;
1. Run boardTracker.py from the boardTracker directory
    NOTE: If root is different from the boardTracker directory, file path issues will appear
	  This will be corrected in future iterations
2. Currently, all printed documents are saved in the out directory
    - Burndown chart generation is currently non-functioning
3. To transfer data from the phone to the board tracker;
    3.1. Ensure both the phone and boardTracker are connected to the same network
    3.2. Start the server on boardTracker
    3.3. Start the client server on the phone
    3.4. It should find each other and transfer data. It will close connections automatically.

current to-do;


To-do;
- UPDATE item button / functionality
- test server connection
- Metric Generator (release and sprint burndown charts)

Persistent Object Naming conventions;
- first letter corresponds to the type, e.g. string is, sObject
- camel case, e.g. sObjectWithALongName
- only applies to persistent objects. temporary objects do not need it if the use is short
    - e.g. for i in range(listObject): etc.
- Type list:
    barcode = bc
    bool = b
    button = bu
    canvas = c
    combobox = cb
    dict = d
    frame = f
    function = fu
    intCtrl = ic
    list = list
    listbox = lb
    listctrl = lc
    menu = m
    menubar = mb
    panel = pa
    paragraph = p
    preformatted = p
    number = n
    server = se
    sizer = si
    socket = so
    static text = st
    string = s
    text control = tc
    text object = t

Issues to fix;
-
