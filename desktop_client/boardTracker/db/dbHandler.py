import sqlite3

from defs import Def
from obj.item import Item
from obj.header import Header
from obj.iteration import Iteration
from obj.transaction import Transaction
from obj.epic import Epic

class DBHandler:

    def __init__(self, name='agileBoardTracker'):
        self.sName = name + '.db'
        self.setup()

    def setup(self):
        """"
        Sets up the database, and verifies it is built correctly
        Could be modified for saving to specific locations
        """
        conn = sqlite3.connect(self.sName)
        c = conn.cursor()

    # create table statements -----------------------------------------------------------------------
        # TRANSACTION
        c.execute("CREATE TABLE IF NOT EXISTS \"transaction\" ("
                "transaction_id INTEGER UNIQUE NOT NULL PRIMARY KEY,"
                "transaction_oldHeader STRING NOT NULL,"
                "transaction_newHeader STRING NOT NULL,"
                "iterationItem_id INTEGER NOT NULL,"
                "transaction_date DATE NOT NULL,"
                "FOREIGN KEY (iterationItem_id) REFERENCES iterationItem (iterationItem_id))")

        # ITERATIONITEM
        c.execute("CREATE TABLE IF NOT EXISTS iterationItem ("
                "iterationItem_id INTEGER UNIQUE NOT NULL PRIMARY KEY,"
                "item_id INTEGER NOT NULL,"
                "iteration_id INTEGER NOT NULL,"
                "FOREIGN KEY (item_id) REFERENCES item (item_id),"
                "FOREIGN KEY (iteration_id) REFERENCES iteration (item_id))")

        # ITERATION
        c.execute("CREATE TABLE IF NOT EXISTS iteration ("
                "iteration_id INTEGER UNIQUE NOT NULL PRIMARY KEY,"
                "iteration_name STRING NOT NULL,"
                "iteration_startDate DATE NOT NULL,"
                "iteration_endDate DATE NOT NULL)")

        # HEADER
        c.execute("CREATE TABLE IF NOT EXISTS header ("
                "header_id INTEGER UNIQUE NOT NULL PRIMARY KEY,"
                "header_name STRING NOT NULL)")

        # ITEM
        c.execute("CREATE TABLE IF NOT EXISTS item ("
                "item_id INTEGER UNIQUE NOT NULL PRIMARY KEY,"
                "item_name STRING NOT NULL,"
                "item_category STRING,"
                "header_name STRING,"
                "epic_name STRING,"
                "item_points INTEGER,"
                "item_description STRING)")

        # EPIC
        c.execute("CREATE TABLE IF NOT EXISTS epic ("
                "epic_id INTEGER UNIQUE NOT NULL PRIMARY KEY,"
                "epic_name STRING NOT NULL)")
        
        conn.commit()
        c.close()

        # see if headers is empty. If it is, add New header
        headers = self.getAll(Def.TABLE_HEADER)
        if headers is False:
            # h = Header(0,'New',0)
            h = Header(0,'New')
            self.insert(Def.TABLE_HEADER, h)

    def get(self, table, sid):
        """
        Gets one entry from a table equal to id
        Return False if failed to find
        """
        statement = "SELECT * FROM {} WHERE {} = {}"
        t = Def.getTableString(table=table)
        column = Def.getTableString(table=table, sid=True)
        # connect to db
        conn = sqlite3.connect(self.sName)
        c = conn.cursor()
        # get data
        data = c.execute(statement.format(t,column,sid)).fetchone()
        # get correct data type
        result = Def.getTableObject(table=table)
        # if data found
        if data:
            result.setFromList(data)
            conn.commit()
        else:
            result = False
        c.close()
        return result            

    def getByName(self, table, name):
        """
        Gets one entry from a table equal to name
        Return False if failed to find
        """
        statement = "SELECT * FROM {} WHERE {} = '{}'"
        t = Def.getTableString(table=table)
        column = Def.getTableString(table=table, name=True)
        # connect to db
        conn = sqlite3.connect(self.sName)
        c = conn.cursor()
        # get data
        data = c.execute(statement.format(t,column,name)).fetchone()
        # get correct data type
        result = Def.getTableObject(table=table)
        # if data found
        if data:
            result.setFromList(data)
            conn.commit()
        else:
            result = False
        c.close()
        return result  

    def getAll(self, table, ids=False):
        """
        Gets all entries of table, return as a list of objects
        if ids is set True, it will only return ids
        """
        result = []
        sTable = Def.getTableString(table=table)
        if sTable:
            column = Def.getTableString(table=table, sid=True) if ids else "*"               
            statement = "SELECT {} FROM {};".format(column,sTable) 
            # connect to db
            conn = sqlite3.connect(self.sName)
            c = conn.cursor()
            # get data
            data = c.execute(statement)
            rows = data.fetchall()
            conn.commit()
            c.close()
            
            if rows:
                for row in rows:
                    obj = Def.getTableObject(table=table)
                    obj.setFromList(row)
                    result.append(obj)
            else:
                result = False
        else:
            result = False
        return result

    def insert(self, table, data):
        """
        Inserts data object into Table

        # check if data already exists, if yes, run update on it
        """
        # print(data.getAsList())
        # exists = self.get(table, data.sSid)
        # # exists=False
        # if exists:
        #     print('exists')
        #     self.update(table, exists)
        # else:
        #     print('not exists')
        conn = sqlite3.connect(self.sName)
        c = conn.cursor()

        statement = "INSERT INTO {} ({}) VALUES ({})"
        sid = self.getNextSid(table)
        name = Def.getTableString(table=table)
        cols = data.getColumnsString()

        if table == Def.TABLE_HEADER:
            values = "'{}','{}'".format(sid, data.sName)             
        elif table == Def.TABLE_ITEM:
            values = "'{}','{}','{}','{}','{}',{},'{}'".format(
                sid,
                data.sName,
                data.sCategory,
                data.sStatus,
                data.sEpic,
                int(data.sPoints),
                data.sDesc
            )
        elif table == Def.TABLE_ITERATION:
            values = "'{}', '{}', '{}', '{}'".format(sid, data.sName, data.sStart, data.sEnd)
        elif table == Def.TABLE_TRANSACTION:
            values = "'{}','{}','{}','{}','{}'".format(sid,data.sOld,data.sNew,int(data.nIterationItem),data.sDate)
        elif table == Def.TABLE_EPIC:
            values = "'{}','{}'".format(sid, data.sName)

        c.execute(statement.format(name,cols,values))
        conn.commit()
        
        c.close()

    def insertMany(self, table, data=[], byNames=False):
        """
        Inserts multiple of table
        Data is a list of objects of table

        # separate into new headers and old headers
        # run insert on new headers, run update on old headers
        """
        if byNames:
            for d in data:
                n = self.getByName(table, d.sName)
                if not n:
                    self.insert(table,d)
        else:
            for d in data:
                self.insert(table,d)

    def update(self, table, data):
        """
        Updates entry of table equal to data
        """
        exists = self.get(table, int(data.sSid))

        # if entry exists
        if exists:
            conn = sqlite3.connect(self.sName)
            c = conn.cursor()
            
            statement = "UPDATE {} SET {} WHERE {} = '{}'"
            
            name = Def.getTableString(table=table)
            
            if name:
                nId = int(data.sSid)
                columns = data.getUpdateColumnsString()
                sId = Def.getTableString(table=table, sid=True)

                print(statement.format(name,columns,sId,nId))
                c.execute(statement.format(name,columns,sId,nId))
                conn.commit()

            c.close()        
        # otherwise insert instead
        else:
            self.insert(table, data)

    def updateMany(self, table, data=[]):
        """
        Updates multiple of table
        Data is a list of objects of table
        """
        for d in data:
            self.update(table,d)

    def delete(self, table, sid):
        """
        Deletes entry of table with id of x
        """
        exists = self.get(table, sid)
        if exists:
            conn = sqlite3.connect(self.sName)
            c = conn.cursor()
            i = Def.getTableString(table=table, sid=True)
            t = Def.getTableString(table=table)
            if t:
                c.execute("DELETE FROM {} WHERE {} = '{}'".format(t,i,sid))
                conn.commit()
            c.close()

    def deleteByName(self, table, name):
        """
        Deletes entry of table with name of x
        """ 
        exists = self.isNameInTable(table, name)
        if exists:
            conn = sqlite3.connect(self.sName)
            c = conn.cursor()
            i = Def.getTableString(table=table, name=True)
            t = Def.getTableString(table=table)
            if t:
                c.execute("DELETE FROM {} WHERE {} = '{}'".format(t,i,name))
                conn.commit()
            c.close()      

    def deleteMany(self, table, data=[], byNames=False):
        """
        Delete multiple values from table
        """
        if byNames:
            for i in data:
                self.deleteByName(table, i)
        else:
            for i in data:
                self.delete(table, i)            

    def isNameInTable(self, table, name):
        """
        Checks if the name offered exists in the table
        NOTE: it is case sensitive
        """
        names = []
        rows = self.getAll(table)
        if rows:
            for row in rows:
                names.append(row.sName)
        return True if name in names else False

    def getNextSid(self, table):
        """
        Get the first unused id in the table, starting from 0
        """
        data = self.getAll(table)
        
        if data:
            sids = []
            for item in data:
                sids.append(int(item.sSid))
            
            sids.sort()
            lst = set(sids)
            diff = set(range(0,max(lst))).difference(lst)
            if len(diff) == 0:
                i = sids[-1] + 1
            else:
                x = list(diff)
                x.sort()
                i = x[0]
            return i
        else:
            return 0