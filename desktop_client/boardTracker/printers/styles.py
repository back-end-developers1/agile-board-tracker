from reportlab.lib.styles import (ParagraphStyle, getSampleStyleSheet)

class Styles:

    # Defines paragraph styles

    def __init__(self):
        styles = getSampleStyleSheet()
        self.nameStyle = ParagraphStyle('Name',
                                    parent=styles['Title'],
                                    alignment=0,
                                    fontSize=18)

        self.catStyle = ParagraphStyle('Category',
                                    parent=styles['Normal'],
                                    alignment=0,
                                    fontSize=16)

        self.descStyle = ParagraphStyle('Description',
                                    parent=styles['Normal'],
                                    alignment=0,
                                    fontSize=14)

        self.pointStyle = ParagraphStyle('Points',
                                    parent=styles['Title'],
                                    alignment=0,
                                    fontSize=18)