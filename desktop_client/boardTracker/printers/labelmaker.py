import  os, qrcode, textwrap, webbrowser, platform

from .styles import Styles
from defs import Def

from reportlab.graphics.barcode import code39, createBarcodeDrawing
from reportlab.lib.colors import blue, black
from reportlab.lib.pagesizes import A4, letter, landscape
from reportlab.lib.units import mm
from reportlab.pdfgen import canvas
from reportlab.pdfbase.pdfmetrics import stringWidth
from reportlab.platypus import Paragraph, Preformatted

STYLES = Styles()

class LabelMaker:

    # vars
    # reportlab uses real world measurements
    # the integer is representative of the real world measurement when multiplied by mm
    # e.g. 51 * mm, is 51mm real life
    # nX = -100 * mm
    # nY = nPadding
    # nBarWidth = 2.5

    # decimals represent a percentage up the page the object will appear at
    # e.g.  0% is the bottom
    #       100% is the top

    CENTRE = 'centre'
    
    def __init__(self):
        pass


# ~~~~ DRAW ITEMS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def draw_items(self, items=[], sName="test"):
        
        n = os.path.join('out', sName + '.pdf')

        pageSize = A4
        cCanvas = canvas.Canvas(n, pagesize=pageSize)

        # label width and height must be less or equal to sheet size
        nLabelW = 100 * mm
        nLabelH = 70 * mm
        nRows = 4

        # figures out how many labels can fit
        nPadding = int(pageSize[0] - (nLabelW*2)) / 3

        nCols = int(pageSize[0]/nLabelW)
        nLabels = nCols * nRows

        # vars
        nBarHeight = 10 * mm
        nX = nPadding
        nY = nPadding
        nPadLabel = 5 * mm
        nRadius = 5

        cCanvas.setLineWidth(4)
        i = 0
        if items:
            for item in items:
                if i is nLabels:
                    cCanvas.showPage()
                    nX = nPadding
                    nY = nPadding
                    i = 0
                
                nPos = 0.85
                nPosCat = 0.7
                nPosDesc = 0.6
                # ------- NAME, CATEGORY --------------------------------------------
                sName = '#' + str(item.sSid) + ' - ' + str(item.sName)
                nParagraphW = 25
                nLines = 2
                # wrap text if it exceeds paragraph width
                if len(sName) > nParagraphW:
                    wt = textwrap.wrap(sName, width=nParagraphW)
                    wt = wt[:nLines]
                    for r in range(len(wt)):
                        nPos = nPos - (r*0.1)
                        pParagraph = Paragraph(wt[r], style=STYLES.nameStyle)
                        pParagraph.wrapOn(cCanvas, nLabelW, nLabelH)
                        pParagraph.drawOn(cCanvas, nX+nPadLabel, nY + (nLabelH*(nPos)))
                else:
                    pParagraph = Paragraph(sName, style=STYLES.nameStyle)
                    pParagraph.wrapOn(cCanvas, nLabelW, nLabelH)
                    pParagraph.drawOn(cCanvas, nX+nPadLabel, nY + (nLabelH*nPos))

                if nPos == 0.85:
                    nPosCat = 0.8
                pParagraph = Paragraph(item.sCategory, style=STYLES.catStyle)
                pParagraph.wrapOn(cCanvas, nLabelW, nLabelH)
                pParagraph.drawOn(cCanvas, nX+nPadLabel, nY + (nLabelH*nPosCat))
                
                # -------- POINTS ------------------------------------------------------
                
                cCanvas.setLineWidth(2)
                cCanvas.setStrokeColor(black)            
                cCanvas.circle(nX + nLabelW * 0.9, nY + nLabelH * 0.85, 16)
                sPoints = str(item.sPoints)
                if len(sPoints) is 1:
                    sPoints = ' ' + str(sPoints)
                pParagraph = Preformatted(sPoints, style=STYLES.pointStyle)
                pParagraph.wrapOn(cCanvas, nLabelW, nLabelH)
                pParagraph.drawOn(cCanvas,   nX + nLabelW * 0.9 - STYLES.pointStyle.fontSize/2,
                                        nY + nLabelH*0.85 - STYLES.pointStyle.fontSize/2)

                # -------- BARCODE ---------------------------------------------------------------------------

                # barX = nX + nLabelW * 0.07
                # barY = nY + nLabelH * 0.07
                barX = nX + nLabelW * 0.12
                barY = nY + nLabelH * 0.14                
                #barY = nY
                
                # nBarHeight = nLabelH * 0.3
                sid = str(item.sSid) if len(str(item.sSid)) > 1 else '0' + str(item.sSid)
                barW = 4 - (len(sid) - 1) * 0.5
                nBarHeight = nLabelH * 0.3
                self.drawBarcode(cCanvas,sid,nBarHeight,barW,barX,barY)
                
                # ------- DESCRIPTION ----------------------------------------------------------

                nPos = nPos - 0.1
                tTextObject = cCanvas.beginText()
                tTextObject.setTextOrigin(nX+nPadLabel, nY + (nLabelH*nPosDesc))
                tTextObject.setFont('Times-Roman', 13)
                nParagraphW = 44
                nLines = 3 # max nLines
                # wrap text if it exceeds paragraph width
                if len(item.sDesc) > nParagraphW:
                    wt = textwrap.wrap(item.sDesc, width=nParagraphW)
                    wt = wt[:nLines]
                    for r in range(len(wt)):
                        tTextObject.textLine(wt[r])
                else:
                    tTextObject.textLine(item.sDesc)
                cCanvas.drawText(tTextObject)

                # ------- LINES -----------------------------------------------------------------------------
                
                # border
                cCanvas.setLineWidth(8)
                cCanvas.setStrokeColor(blue)
                cCanvas.rect(nX, nY, nLabelW, nLabelH)

                # ----- DEFINE nX AND nY ---------------------------------------------------------

                i += 1
                if i % 2 is 0:  # even
                    nX = nPadding
                    nY += nLabelH + nPadding
                else:           # odd
                    nX += nLabelW + nPadding
            
            cCanvas.save()
            # open the file
            webbrowser.open(n)
        else:
            print('No items to print')

# ~~ DRAW HEADER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def draw_headers(self, headers=[], sName="headers"):

        n = os.path.join('out', sName + '.pdf')
        
        pageSize = landscape(A4)
        cCanvas = canvas.Canvas(n, pagesize=pageSize)

        nPadding = 10 * mm
        nRows = 1

        nTextY = pageSize[1] * 0.65
        nFontSize = 140

        nBarHeight = 50 * mm
        nBarcodeY = pageSize[1] * 0.25
        nBarWidthbase = 7

        i = 1
        if headers:
            for header in headers:
    
    # --------- BARCODE ------------------------------------------------------------------------------
                nBarWidth = nBarWidthbase - (len(str(header.sSid)) - 1) * 0.5
                x = self.CENTRE
                y = self.CENTRE
                sid = str(header.sSid) if len(str(header.sSid)) > 1 else '0' + str(header.sSid)               

                self.drawBarcode(cCanvas,sid,nBarHeight,nBarWidth,x,y,nPadding,pageSize[0],pageSize[1])
                
                textWidth = stringWidth(header.sName,'Times-Roman', nFontSize)


    # --------- TITLE ------------------------------------------------------------------------------------
                tText = cCanvas.beginText()
                nNewFontSize = nFontSize
                while(textWidth > pageSize[1]):
                    nNewFontSize = nNewFontSize - 5
                    textWidth = stringWidth(header.sName,'Times-Roman', nNewFontSize)                    
                tText.setFont('Times-Roman', nNewFontSize)
                tText.setTextOrigin((pageSize[0] - textWidth) / 2.0 - nPadding, nPadding + nTextY)
                tText.textLine(header.sName)
                cCanvas.drawText(tText)


    # --------- BORDER -------------------------------------------------------------------------------------------
                borderY = 0.2
                borderH = 0.7
                cCanvas.setLineWidth(1)
                cCanvas.setStrokeColor(black)
                cCanvas.rect(nPadding,pageSize[1]*borderY,pageSize[0]-(nPadding*2),pageSize[1]*borderH)
                
    # --------- NEW PAGE -------------------------------------------------------------------------------------
                # if greater than rows per page, create new page
                i += 1
                if i > nRows:
                    # adds a new page, the function is a misnomer
                    cCanvas.showPage()
                    # nY = nPadding
                    i = 1

    # ----- SAVE AND OPEN -------------------------------------------------------------
            # creates the pdf, aka saves it
            cCanvas.save()
            
            # open the file
            webbrowser.open(n)
        else:
            print('No headers to print')


# ~~~~ DRAW BARCODE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def drawBarcode(self, canvas, sid, barHeight, barWidth, x=0, y=0, padding=0, pageW=A4[0],pageH=A4[1]):
        bcCode = code39.Standard39(sid, barHeight=barHeight, barWidth=barWidth, checksum=0)
        bcCode.lquiet = 0
        bcCode.quiet = 0

        if x == self.CENTRE:
            x = (pageW - bcCode.width) / 2.0 - padding
        if y == self.CENTRE:
            y = (pageH - bcCode.height) / 2.0 - padding
        bcCode.drawOn(canvas, x, y)