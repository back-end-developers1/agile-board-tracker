import socket, json, threading

from db.dbHandler import DBHandler
from obj.header import Header
from obj.item import Item
from obj.transaction import Transaction
from defs import Def

from datetime import date

class Server(threading.Thread):
    def __init__(self, group=None,target=None, name=None, updateOutput=None, database=None, updateFlag=None):
        threading.Thread.__init__(self,group=None,target=None,name=None)

        self.bQuit = False
        self.bLISTENING = False
        self.fuUpdateOutput = updateOutput
        self.fuUpdateFlag = updateFlag
        if database == None:
            self.db = DBHandler()
        else:
            self.db = database

    def run(self):
        self.startServer()

    def startServer(self, timeout=100, port=10000):
        self.nTimeout = timeout
        self.nPort = port
        self.soServer = None
        self.conn = None
        self.addr = None

        self.sIP = self.getIP()
        self.setupServer()
        self.getClient()
        print('after client')
        self.getData()
        self.closeConnections()

    def getIP(self, pipeOut=True):
        self.print('Obtaining IP address...', pipeOut)
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setblocking(False)
        s.connect(('10.255.255.255',1))
        ip = s.getsockname()[0]
        s.close()
        return ip

    def setupServer(self):
        self.print('Setting up socket...')
        self.soServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.soServer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.print('Setting timeout to ' + str(self.nTimeout))
        self.soServer.settimeout(self.nTimeout)
        self.print('Binding server to ' + str(self.sIP) + ':' + str(self.nPort))
        self.soServer.bind((self.sIP, self.nPort))
        self.soServer.listen(5)
        self.print('Server established')

    def print(self, msg, pipeOut=True):
        # sends the print message to the passed in function
        if pipeOut:
            print(msg)
        if self.fuUpdateOutput:
            self.fuUpdateOutput(msg)
    
    def getPrintString(self, data):
        p = ''
        if isinstance(data, list):
            p = ' '.join(map(str,data))
        elif isinstance(data, dict):
            p = ' '.join('%s:%s' % (key, value) for (key, value) in data.items())
        return p

    def getClient(self):
        self.bLISTENING = True
        self.print('Waiting for connections...')
        self.conn, self.addr = self.soServer.accept()
    
    def getData(self):
        if not self.bQuit:
            self.bLISTENING = False
            self.print('Receiving data from ' + str(self.addr[0]))
            data = self.conn.recv(4096).decode('utf-8')
            self.print('data: ' + str(data))
            jdata = json.loads(data)
            self.print('Data received')
            self.print('jData: ' + self.getPrintString(jdata))

            if jdata:
                for k,v in jdata.items():
                    newHeader = self.db.get(Def.TABLE_HEADER, v)
                    if newHeader:
                        item = self.db.get(Def.TABLE_ITEM, k)
                        oldHeader = item.sStatus
                        item.sStatus = newHeader.sName
                        self.db.update(Def.TABLE_ITEM, item)
                        self.db.insert(Def.TABLE_TRANSACTION, Transaction(0,oldHeader,newHeader.sName,0,date.today()))
                self.fuUpdateFlag()

    def closeConnections(self):
        self.bQuit = True
        self.print('Closing connections...')
        if self.bLISTENING:
            try:
                socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((self.sIP, self.nPort))
            except ConnectionRefusedError as e:
                pass
        close = threading.Thread(target=self.close)
        close.start()
        self.print('Session terminated')

    def close(self):
        if self.conn:
            self.conn.close()
        if self.soServer:
            self.soServer.close()
