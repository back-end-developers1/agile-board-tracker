import wx

from defs import Def
from obj.header import Header

class HeaderFrame(wx.MiniFrame):

    def __init__(self, title, parent=None, database=None):
        wx.MiniFrame.__init__(self, parent=parent, title=title, style=wx.CAPTION | wx.RESIZE_BORDER)
        
        self.db = database
        
        nWIDTH = 400
        nHEIGHT = 350
        nPADDING = 50
        self.SetSize(nWIDTH + nPADDING, nHEIGHT)
        paPanel = wx.Panel(self, wx.ID_ANY)
        self.nIndex = 0

        stDesc = wx.StaticText(paPanel)
        stDesc.SetLabel(Def.INFO_HEADER)

        self.listHeaders = self.getHeaders()
        self.deletedHeaders = []
        self.updatedHeaders = []

        self.lbHeaders = wx.ListBox(
            paPanel, 
            size=(nWIDTH/2,-1),
            style=wx.LC_REPORT | wx.BORDER_SUNKEN,
            choices = self.listHeaders
            )

    # -- ADD, EDIT, DELETE ------------------------------------------------------
        self.tcAdd = wx.TextCtrl(paPanel, size=(nWIDTH/2,-1))
        buAdd = wx.Button(paPanel, label=Def.BUTTON_ADD)
        buAdd.Bind(wx.EVT_BUTTON, self.addClicked)
        self.tcEdit = wx.TextCtrl(paPanel, size=(nWIDTH/2,-1))
        buEdit = wx.Button(paPanel, label=Def.BUTTON_UPDATE)
        buEdit.Bind(wx.EVT_BUTTON, self.editClicked)       
        buDelete = wx.Button(paPanel, label=Def.BUTTON_DELETE)
        buDelete.Bind(wx.EVT_BUTTON, self.deleteClicked)
    # -- SAVE, CANCEL -----------------------------------------------------------
        buSave = wx.Button(paPanel, label=Def.BUTTON_SAVE)
        buSave.Bind(wx.EVT_BUTTON, self.saveClicked)
        buCancel = wx.Button(paPanel, label=Def.BUTTON_CANCEL)
        buCancel.Bind(wx.EVT_BUTTON, self.cancelClicked)
    # -- RIGHT PANEL ------------------------------------------------------------
        siButtons = wx.BoxSizer(wx.VERTICAL)
        siButtons.Add(self.tcAdd, 0, wx.ALL|wx.EXPAND, 1)
        siButtons.Add(buAdd, 0, wx.ALL|wx.CENTER, 5)
        siButtons.Add(self.tcEdit, 0, wx.ALL|wx.EXPAND, 1)
        siButtons.Add(buEdit, 0, wx.ALL|wx.CENTER, 1)
        siButtons.Add(buDelete, 0, wx.ALL|wx.CENTER, 1)
        siButtons.AddSpacer(10)        
        siButtons.Add(buSave, 0, wx.ALL|wx.CENTER, 1)
        siButtons.Add(buCancel, 0, wx.ALL|wx.CENTER, 1)
    # -- CONTENT ----------------------------------------------------------------
        siContent = wx.BoxSizer(wx.HORIZONTAL)
        siContent.Add(self.lbHeaders, 1, wx.ALL|wx.EXPAND, 5)
        siContent.Add(siButtons, 1, wx.ALL|wx.CENTER, 5)
    # -- MAIN SIZER -------------------------------------------------------------
        siSizer = wx.BoxSizer(wx.VERTICAL)
        siSizer.Add(stDesc, 0, wx.ALL|wx.CENTER,5)
        siSizer.Add(siContent, 1, wx.ALL|wx.LEFT, 5)

        paPanel.SetSizer(siSizer)

    def addClicked(self, event):
        line = self.tcAdd.GetValue()
        if line == '':
            line = 'Unnamed' + str(self.nIndex)
        if line not in self.lbHeaders.GetStrings():
            self.lbHeaders.Append(line)
            self.nIndex += 1

    def editClicked(self, event):
        i = self.lbHeaders.GetSelection()
        if i != wx.NOT_FOUND:
            line = self.tcEdit.GetValue()
            if line != '':
                if line not in self.lbHeaders.GetStrings():
                    if line in self.updatedHeaders:
                        self.updatedHeaders.remove(line)
                    self.updatedHeaders.append(self.lbHeaders.GetStringSelection())
                    self.lbHeaders.SetString(i, line)

    def deleteClicked(self, event):
        i = self.lbHeaders.GetSelection()
        if i != wx.NOT_FOUND:
            self.deletedHeaders.append(self.lbHeaders.GetStringSelection())
            self.lbHeaders.Delete(i)

    def saveClicked(self, event):
        self.saveHeaders()
        self.resetTextControls()
        self.Hide()
    
    def saveHeaders(self):
        h = self.lbHeaders.GetItems()
        headers = []
        if h:
            for i in range(len(h)):
                sid = 0
                name = h[i]
                headers.append(Header(sid, name))

        self.deletedHeaders.extend(self.updatedHeaders)

        self.db.deleteMany(Def.TABLE_HEADER, self.deletedHeaders, byNames=True)
        self.db.insertMany(Def.TABLE_HEADER, headers, byNames=True)
        self.deletedHeaders = []
        self.updatedHeaders = []

    def cancelClicked(self, event):
        self.lbHeaders.Clear()
        # clear list and load original list
        self.loadHeaders()
        self.resetTextControls()
        self.Hide()
    
    def loadHeaders(self):
        self.lbHeaders.Clear()
        self.deletedHeaders = []
        headers = self.getHeaders()
        if headers:
            self.lbHeaders.InsertItems(headers,0)

    def resetTextControls(self):
        self.tcAdd.SetValue('')
        self.tcEdit.SetValue('')
    
    def getHeaders(self):
        headers = []
        h = self.db.getAll(Def.TABLE_HEADER)
        if h:
            for header in h:
                headers.append(header.sName)
        return headers