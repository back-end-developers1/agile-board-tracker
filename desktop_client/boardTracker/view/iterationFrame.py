import wx

from defs import Def
from obj.header import Header

class IterationFrame(wx.MiniFrame):

    def __init__(self, title, parent=None, database=None):
        wx.MiniFrame.__init__(self, parent=parent, title=title, style=wx.CAPTION | wx.RESIZE_BORDER)
        
        self.db = database
        
        nWIDTH = 400
        nHEIGHT = 350
        nPADDING = 50
        self.SetSize(nWIDTH + nPADDING, nHEIGHT)
        paPanel = wx.Panel(self, wx.ID_ANY)

        buCancel = wx.Button(paPanel, label=Def.BUTTON_CANCEL)
        buCancel.Bind(wx.EVT_BUTTON, self.cancelClicked)

    def cancelClicked(self, event):
        # clear list and load original list
        self.Hide()