import os, wx, subprocess, sys, socket, threading, webbrowser
import itertools as it

from .headerFrame import HeaderFrame
from .serverFrame import ServerFrame
from .iterationFrame import IterationFrame
from .addFrame import AddFrame

from defs import Def
from obj.item import Item
from obj.header import Header
from obj.iteration import Iteration

from printers.labelmaker import LabelMaker
from server.server import Server
from db.dbHandler import DBHandler

class Gui():
    # creates an app to house the frame
    def __init__(self):
        app = wx.App()
        frame = MainFrame()
        app.MainLoop()

class MainFrame(wx.Frame):
    # creates a frame to house the paPanel
    def __init__(self):
        wx.Frame.__init__(self, parent=None, id=wx.ID_ANY,
                          title=Def.TITLE_DESKTOP, size=(800,600))

        icon = wx.Icon()
        icon.CopyFromBitmap(wx.Bitmap("application_icon.png",wx.BITMAP_TYPE_PNG))
        self.SetIcon(icon)

        paPanel = MainPanel(self)
        self.Show()

class MainPanel(wx.Panel):
    # houses all of the gui bits
    def __init__(self, parent):
        wx.Panel.__init__(self, parent=parent, id=wx.ID_ANY)

        FONT_TITLE = wx.Font(12,wx.FONTFAMILY_DEFAULT,wx.FONTSTYLE_NORMAL,wx.FONTWEIGHT_NORMAL)

        self.db = DBHandler()

# ----- VIEWS AND ITEMS ------------------------------------------------------

        # views
        self.lcViewItems = None
        self.lbViewItr = wx.ListBox(self, size = (100,-1), choices=[], style=wx.LB_EXTENDED)
        self.Bind(wx.EVT_LISTBOX, self.itrSelectEvent, self.lbViewItr)
        self.setupView()

        # popup frames
        self.fAdd = AddFrame(title=Def.TITLE_ADD, parent=self)
        self.fServer = ServerFrame(title=Def.TITLE_SERVER, parent=self)
        self.fHeader = HeaderFrame(title=Def.TITLE_HEADER, parent=self, database=self.db)
        self.fIteration = IterationFrame(title=Def.TITLE_ITERATION, parent=self, database=self.db)

# ----- mbMenu ---------------------------------------------------------------------------
        mbMenu = wx.MenuBar()
        mFile = wx.Menu()
        mServer = wx.Menu()
        mHeader = wx.Menu()
        mIteration = wx.Menu()
        mPrint = wx.Menu()
        mHelp = wx.Menu()

        mbMenu.Append(mFile, Def.MENU_FILE)
        mbMenu.Append(mServer, Def.MENU_SERVER)
        mbMenu.Append(mHeader, Def.MENU_HEADER)
        mbMenu.Append(mIteration, Def.MENU_ITERATION)
        mbMenu.Append(mPrint, Def.MENU_PRINT)
        mbMenu.Append(mHelp, Def.MENU_HELP)

        parent.SetMenuBar(mbMenu)

# ----- BUTTONS ---------------------------------------------------------------------------

        # add item button
        buAdd = wx.Button(self, wx.ID_ADD, Def.BUTTON_ADD_ENTRY)
        buAdd.Bind(wx.EVT_BUTTON, self.addItemEvent)

        # delete item button
        buDelete = wx.Button(self, wx.ID_ANY, Def.BUTTON_DELETE_ENTRY)
        buDelete.Bind(wx.EVT_BUTTON, self.deleteItemEvent)
        buDelete.SetBackgroundColour(wx.Colour(255,150,150))

        # File Menu
        #file
        mbQuit = mFile.Append(wx.ID_EXIT, Def.FILE_QUIT, 'Quit Application')
        #server
        mbServer = mServer.Append(wx.ID_ANY, Def.FILE_OPEN, 'Open the Server')
        parent.Bind(wx.EVT_MENU, self.serverOpenEvent, mbServer)
        #header
        mbHeaderManage = mHeader.Append(wx.ID_ANY, Def.FILE_MANAGE, 'Manage the headers')
        parent.Bind(wx.EVT_MENU, self.manageHeaderEvent, mbHeaderManage)        
        mbPrintHeaderH = mHeader.Append(wx.ID_ANY, Def.FILE_PRINT, 'Print All Current Headers')
        parent.Bind(wx.EVT_MENU, self.printHeaderEvent, mbPrintHeaderH)     
        #iteration
        mbIterationManage = mIteration.Append(wx.ID_ANY, Def.FILE_MANAGE, 'Manage the Iterations')
        parent.Bind(wx.EVT_MENU, self.manageIterationEvent, mbIterationManage)   
        #print
        mbPrintItems = mPrint.Append(wx.ID_ANY, Def.FILE_ITEMS, 'Print Items from Selected Iterations')
        parent.Bind(wx.EVT_MENU, self.printItemEvent, mbPrintItems)
        mbPrintHeaderP = mPrint.Append(wx.ID_ANY, Def.FILE_HEADERS, 'Print All Current Headers')        
        parent.Bind(wx.EVT_MENU, self.printHeaderEvent, mbPrintHeaderP)
        mbPrintBurnSprint = mPrint.Append(wx.ID_ANY, Def.FILE_BURN_SPRINT, 'Print Sprint Burndown Chart')
        mbPrintBurnRelease = mPrint.Append(wx.ID_ANY, Def.FILE_BURN_RELEASE, 'Print Release Burndown Chart')
        #help
        mbHelpManual = mHelp.Append(wx.ID_ANY, Def.FILE_MANUAL, 'User Manual')
        parent.Bind(wx.EVT_MENU, self.helpManualEvent, mbHelpManual)
        mHelp.AppendSeparator()
        mbHelpAbout = mHelp.Append(wx.ID_ANY, Def.FILE_ABOUT, 'About the Team')

# ----- SIZERS ----------------------------------------------------------------------------
        WRAP_WIDTH = 200
        # create sizers
        # sizers are like java swing layout managers

        # Top Panel button controls (add, delete)
        siBtn = wx.BoxSizer(wx.HORIZONTAL)
        siBtn.Add(buAdd, 0, wx.ALL|wx.LEFT,2)
        siBtn.Add(buDelete, 0, wx.ALL|wx.LEFT,2)

        # titles
        stIteration = wx.StaticText(self,-1,Def.TITLE_ITERATIONS)
        stIteration.SetFont(FONT_TITLE)
        stItem = wx.StaticText(self,-1,Def.TITLE_ITEMS)
        stItem.SetFont(FONT_TITLE)

        # iterations
        siIteration = wx.BoxSizer(wx.VERTICAL)
        siIteration.Add(stIteration, 0, wx.ALL|wx.LEFT,5)
        siIteration.Add(self.lbViewItr, 1, wx.ALL|wx.EXPAND,5)
        # items
        siItem = wx.BoxSizer(wx.VERTICAL)
        siItem.Add(stItem, 0, wx.ALL|wx.LEFT,5)
        siItem.Add(self.lcViewItems, 1, wx.ALL|wx.EXPAND,5)

        siContent = wx.BoxSizer(wx.HORIZONTAL)
        siContent.Add(siIteration, 0, wx.ALL|wx.EXPAND,2)
        siContent.Add(siItem, 1, wx.ALL|wx.EXPAND,2)

        # main sizer
        siMain = wx.BoxSizer(wx.VERTICAL)
        siMain.Add(siBtn, 0, wx.ALL|wx.EXPAND,0)
        siMain.Add(siContent, 1, wx.ALL|wx.EXPAND,0)
        self.SetSizer(siMain)
        self.Layout()

# ----- EVENTS -----------------------------------------------------------------

    def addItemEvent(self, event):
        self.fAdd.load()

    def deleteItemEvent(self, event):
        i = self.lcViewItems.GetFirstSelected()
        if i is not -1:
            sel = [i]
            # get all selected items
            while len(sel) != self.lcViewItems.GetSelectedItemCount():
                i = self.lcViewItems.GetNextSelected(i)
                sel.append(i)
            items = []
            # get ids to delete
            for x in sel:
                items.append(self.lcViewItems.GetItem(x).Text)
            self.db.deleteMany(Def.TABLE_ITEM,items)
            self.updateItems()
            self.updateIterations()

    def printItemEvent(self, event):
        # define name and items
        name = 'release'
        items = []
        selectedItems = self.getSelectedIterations()
        allItems = self.db.getAll(Def.TABLE_ITEM)
        # if *ALL* or nothing has been selected
        if Def.ALL in selectedItems:
            items = allItems
        else:
            # get iteration name and items
            for item in allItems:
                if item.sIteration in selectedItems:
                    items.append(item)
            name = '_'.join(it)
        # create the label pdf
        maker = LabelMaker()
        maker.draw_items(items, name)

    def printHeaderEvent(self, event):
        name = 'headers'
        maker = LabelMaker()
        headers = self.db.getAll(Def.TABLE_HEADER)
        headersToPrint = []
        # for header in headers:
        #     if header.bActive:
        #         headersToPrint.append(header)
        maker.draw_headers(headers)

    def manageHeaderEvent(self, event):
        self.fHeader.Show()

    def serverOpenEvent(self, event):
        self.fServer.Show()

    def itrSelectEvent(self, event):
        self.updateItems()
    
    def manageIterationEvent(self, event):
        self.fIteration.Show()
    
    def helpManualEvent(self, event):
        file = os.path.join('documentation', 'AgileBoardTracker_userManual.pdf')
        webbrowser.open(file)

# ----- METHODS --------------------------------------------------------------------------------

    def getHeaders(self):
        # replace with db handler functions
        return self.db.getAll(Def.TABLE_HEADER)

    # sets up the iteration and item view panels
    def setupView(self):
        # iterations
        self.updateIterations()
        self.lbViewItr.SetSelection(0)
        
        # items
        c = Item.getColumns(self)
        self.lcViewItems = wx.ListCtrl(self, style=wx.LC_REPORT)
        for k,v in c.items():
            self.lcViewItems.InsertColumn(k, v[0], width=v[1])
        
        self.updateItems()

    def updateIterations(self):
        its = [Def.ALL]
        iterations = self.db.getAll(Def.TABLE_ITERATION)
        if iterations:
            for iteration in iterations:
                its.append(iteration.sName)
        self.lbViewItr.Set(its)
    
    def updateItems(self):
        # update items lisctrl
        selectedIterations = self.getSelectedIterations()
        self.lcViewItems.DeleteAllItems()
        allItems = self.db.getAll(Def.TABLE_ITEM)
        if allItems:
            if len(allItems) != 0:
            # if def.all then append everything
            # else loop through selections and append only if in selected   
                for item in allItems:
                    if Def.ALL in selectedIterations or item.sIteration in selectedIterations:
                        self.lcViewItems.Append(item.getAsList())

    # gets the currently selected iterations    
    def getSelectedIterations(self):
        sel = self.lbViewItr.GetSelections()
        x = []
        for s in sel:
            x.append(self.lbViewItr.GetString(s))
        if len(x) <= 0:
            x = Def.ALL
        return x
