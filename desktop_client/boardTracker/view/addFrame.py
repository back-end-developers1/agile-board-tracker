import wx

from wx.lib.intctrl import IntCtrl

from defs import Def
from obj.item import Item
from obj.header import Header

class AddFrame(wx.MiniFrame):
 
    def __init__(self, title, parent=None):
        wx.MiniFrame.__init__(self, parent=parent, title=title, size=(380,450), style=wx.CAPTION)

        self.Hide()

        paPanel = wx.Panel(self,id=wx.ID_ANY)

        nWidthLabel = 80
        nHeightLabel = 20
        nWidthTF = 250
        nHeightTF = 100

        siMain = wx.BoxSizer(wx.VERTICAL)
        
        # Name
        siName = wx.BoxSizer(wx.HORIZONTAL)
        stName = wx.StaticText(paPanel,size=wx.Size(nWidthLabel,nHeightLabel))
        stName.SetLabel(Def.LABEL_NAME)
        self.tcName = wx.TextCtrl(paPanel,size=wx.Size(nWidthTF,nHeightLabel))
        siName.Add(stName,0,wx.ALL|wx.LEFT,5)
        siName.Add(self.tcName,0,wx.ALL|wx.RIGHT,5)
        
        # Iteration
        siIter = wx.BoxSizer(wx.HORIZONTAL)
        stIter = wx.StaticText(paPanel,size=wx.Size(nWidthLabel,nHeightLabel))
        stIter.SetLabel(Def.LABEL_ITERATION)
        self.tcIter = wx.TextCtrl(paPanel,size=wx.Size(nWidthTF,nHeightLabel))
        self.tcIter.SetValue(Def.LABEL_BACKLOG)
        siIter.Add(stIter,0,wx.ALL|wx.CENTER,5)
        siIter.Add(self.tcIter,0,wx.ALL|wx.CENTER,5)
        
        # Type / Category
        siCat = wx.BoxSizer(wx.HORIZONTAL)
        stCat = wx.StaticText(paPanel,size=wx.Size(nWidthLabel,nHeightLabel))
        stCat.SetLabel(Def.LABEL_TYPE)
        self.tcCat = wx.TextCtrl(paPanel,size=wx.Size(nWidthTF,nHeightLabel))
        siCat.Add(stCat,0,wx.ALL|wx.CENTER,5)
        siCat.Add(self.tcCat,0,wx.ALL|wx.CENTER,5)
        
        # Header
        siHeader = wx.BoxSizer(wx.HORIZONTAL)
        stHeader = wx.StaticText(paPanel,size=wx.Size(nWidthLabel,nHeightLabel))
        stHeader.SetLabel(Def.LABEL_STATUS)
        self.cbHeader = wx.ComboBox(paPanel, size=wx.Size(nWidthTF,nHeightLabel), choices=self.getHeaderNames(), style=wx.CB_READONLY)
        siHeader.Add(stHeader,0,wx.ALL|wx.CENTER,5)
        siHeader.Add(self.cbHeader,0,wx.ALL|wx.CENTER,5)
        self.cbHeader.SetSelection(0)            
        
        # Epic
        siEpic = wx.BoxSizer(wx.HORIZONTAL)
        stEpic = wx.StaticText(paPanel,size=wx.Size(nWidthLabel,nHeightLabel))
        stEpic.SetLabel(Def.LABEL_EPIC)
        self.tcEpic = wx.TextCtrl(paPanel,size=wx.Size(nWidthTF,nHeightLabel))
        siEpic.Add(stEpic,0,wx.ALL|wx.CENTER,5)
        siEpic.Add(self.tcEpic,0,wx.ALL|wx.CENTER,5)
        
        # Points
        siPoints = wx.BoxSizer(wx.HORIZONTAL)
        stPoints = wx.StaticText(paPanel,size=wx.Size(nWidthLabel,nHeightLabel))
        stPoints.SetLabel(Def.LABEL_POINTS)
        self.icPoints = IntCtrl(paPanel,size=wx.Size(nWidthTF,nHeightLabel))
        siPoints.Add(stPoints,0,wx.ALL|wx.CENTER,5)
        siPoints.Add(self.icPoints,0,wx.ALL|wx.CENTER,5)
        
        # Description
        siDesc = wx.BoxSizer(wx.HORIZONTAL)
        stDesc = wx.StaticText(paPanel,size=wx.Size(nWidthLabel,nHeightLabel))
        stDesc.SetLabel(Def.LABEL_DESC)
        self.tcDesc = wx.TextCtrl(paPanel,wx.ID_ANY,size=wx.Size(nWidthTF,nHeightTF),
                                    style=wx.TE_MULTILINE|wx.TE_BESTWRAP)
        siDesc.Add(stDesc,0,wx.ALL|wx.CENTER,5)
        siDesc.Add(self.tcDesc,2,wx.ALL|wx.CENTER,5)
        
        # Buttons
        siButtons = wx.BoxSizer(wx.HORIZONTAL)
        buAdd = wx.Button(paPanel,wx.ID_ANY,Def.BUTTON_ADD)
        buAdd.Bind(wx.EVT_BUTTON, self.addClicked)
        buCancel = wx.Button(paPanel,wx.ID_ANY,Def.BUTTON_CANCEL)
        buCancel.Bind(wx.EVT_BUTTON, self.cancelClicked)
        buCancel.SetBackgroundColour(wx.Colour(255,150,150))
        siButtons.Add(buAdd,0,wx.ALL|wx.CENTER,5)
        siButtons.Add(buCancel,0,wx.ALL|wx.CENTER,5)

        # build sizer
        siMain.Add(siName,0,wx.ALL|wx.LEFT,5)
        siMain.Add(siIter,0,wx.ALL|wx.LEFT,5)
        siMain.Add(siCat,0,wx.ALL|wx.LEFT,5)
        siMain.Add(siHeader,0,wx.ALL|wx.LEFT,5)
        siMain.Add(siEpic,0,wx.ALL|wx.LEFT,5)
        siMain.Add(siPoints,0,wx.ALL|wx.LEFT,5)
        siMain.Add(siDesc,0,wx.ALL|wx.LEFT,5)
        siMain.Add(siButtons,0,wx.ALL|wx.CENTER,5)

        paPanel.SetSizer(siMain)

    def addClicked(self, event):
        item = Item(sid=0,
                    name=self.tcName.GetValue(),
                    category=self.tcCat.GetValue(),
                    status=self.cbHeader.GetStringSelection(),
                    epic=self.tcEpic.GetValue(),
                    points=self.icPoints.GetValue(),
                    desc=self.tcDesc.GetValue(),
                    iteration=self.tcIter.GetValue()
                    )

        self.Parent.db.insert(Def.TABLE_ITEM, item)
        self.Parent.updateIterations()
        self.Parent.updateItems()
        self.resetValues()
    
    def cancelClicked(self, event):
        self.resetValues()
        self.Hide()

    def load(self):
        self.resetValues()
        self.Show()
    
    def resetValues(self):
        self.updateHeaders()
        self.tcName.SetValue('')
        self.tcCat.SetValue('')
        self.cbHeader.SetSelection(0)
        self.tcEpic.SetValue('')
        self.icPoints.SetValue(0)
        self.tcDesc.SetValue('')
        self.tcIter.SetValue(Def.LABEL_BACKLOG)
    
    def updateHeaders(self):
        names = self.getHeaderNames()
        self.cbHeader.Clear()
        self.cbHeader.Set(names)
        self.cbHeader.SetSelection(0)        

    def getHeaderNames(self):
        names = []
        headers = self.Parent.db.getAll(Def.TABLE_HEADER)
        if headers:
            for header in headers:
                names.append(header.sName)
        else:
            names.append('New')
        return names