import wx

from defs import Def
from server.server import Server

class ServerFrame(wx.MiniFrame):

    def __init__(self, title, parent=None, ip='localhost'):
        wx.MiniFrame.__init__(self, parent=parent, title=title, style=wx.CAPTION)

        self.Hide()

        self.sIP = Server().getIP(pipeOut=False)
        self.seServer = None

        paPanel = wx.Panel(self,id=wx.ID_ANY)

        nBWIDTH = 300
        nBHEIGHT = 200
        nWIDTH = 350
        nHEIGHT = 300
        nPADDING = 50

        siMain = wx.BoxSizer(wx.VERTICAL)
        # Name
        siName = wx.BoxSizer(wx.HORIZONTAL)

        self.tcInfoBox = wx.TextCtrl(paPanel,size=wx.Size(nBWIDTH,nBHEIGHT),style=wx.ALL|wx.TE_READONLY|wx.TE_MULTILINE)
        self.updateOutput()
        siName.Add(self.tcInfoBox,0,wx.ALL|wx.CENTER,5)

        # Buttons
        siButtons = wx.BoxSizer(wx.HORIZONTAL)
        
        self.btnStart = wx.Button(paPanel,wx.ID_ANY,Def.BUTTON_START)
        self.btnStart.Bind(wx.EVT_BUTTON, self.startClicked)

        buCancel = wx.Button(paPanel,wx.ID_ANY,Def.BUTTON_CANCEL)
        buCancel.Bind(wx.EVT_BUTTON, self.cancelClicked)
        buCancel.SetBackgroundColour(wx.Colour(255,150,150))
        
        siButtons.Add(self.btnStart,0,wx.ALL|wx.LEFT,5)
        siButtons.AddSpacer(nPADDING)
        siButtons.Add(buCancel,0,wx.ALL|wx.RIGHT,5)

        # build sizer
        siMain.Add(siName,0,wx.ALL|wx.CENTER,1)
        siMain.Add(siButtons,0,wx.ALL|wx.CENTER,1)

        paPanel.SetSizer(siMain)
        self.SetSize(nWIDTH,nHEIGHT)
    
    def updateOutput(self, msg=None):
        if msg == None:
            self.tcInfoBox.SetValue(Def.INFO_SERVER + self.sIP + '\n' + '-'*58)
        else:
            wx.CallAfter(self.tcInfoBox.AppendText, '\n' + msg)

    def updateDisplay(self):
        self.Parent.updateItems()

    def startClicked(self, event):
        self.seServer = Server(updateOutput=self.updateOutput, updateFlag=self.updateDisplay)
        self.btnStart.Disable()
        self.seServer.start()

    def cancelClicked(self, event):
        if self.seServer:
            self.seServer.closeConnections()
        self.Hide()
        self.btnStart.Enable()
        self.updateOutput()