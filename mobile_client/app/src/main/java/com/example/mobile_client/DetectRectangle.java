package com.example.mobile_client;

import android.graphics.Bitmap;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

class DetectRectangle {
	static Bitmap detect(ArrayList<Rectangle> rectangleList, Bitmap sourceBitmap, Bitmap destinationBitmap,
			Size imageSize, String rectangleType, boolean crop,
			int minHeight, int maxHeight, int minWidth, int maxWidth) {
//        try {
            // load the native library
//            System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
            System.loadLibrary("opencv_java3");  // use this instead in Android

//            System.out.println("Bitmap width is: " + sourceBitmap.getWidth());
//            System.out.println("Bitmap height is: " + sourceBitmap.getHeight());

            // read image in colour
            Mat src = new Mat();
//            src = Imgcodecs.imread(sourceFilePath, Imgcodecs.IMREAD_COLOR);
            Utils.bitmapToMat(sourceBitmap, src);

//            System.out.println("Converted bitmap size is: " + src.size());
            
            Mat srcResized = src.clone();
            Imgproc.resize(src, srcResized, imageSize);
            
            Mat rgb = srcResized.clone();

            // convert to image to RGB
            Imgproc.cvtColor(srcResized, rgb, Imgproc.COLOR_BGR2RGB);

            // apply median blur
            Mat blurred = rgb.clone();
            Imgproc.medianBlur(rgb, blurred, 9);

            // create grayscale mats
            Mat gray0 = new Mat(blurred.size(), CvType.CV_8U);
            Mat gray = new Mat();

            List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

            List<Mat> blurredChannel = new ArrayList<Mat>();
            blurredChannel.add(blurred);
            List<Mat> gray0Channel = new ArrayList<Mat>();
            gray0Channel.add(gray0);

            MatOfPoint2f approxCurve;

            double maxArea = 0;
            int maxId= -1;

            for (int c = 0; c < 3; c++) {
                int ch[] = { c, 0 };
                Core.mixChannels(blurredChannel, gray0Channel, new MatOfInt(ch));

                int thresholdLevel = 1;
                for (int t = 0; t < thresholdLevel; t++) {
                    if (t == 0) {
                        Imgproc.Canny(gray0, gray, 10, 20, 3, true);
                        Imgproc.dilate(gray, gray, new Mat(), new Point(-1, -1), 1);
                    } else {
                        Imgproc.adaptiveThreshold(gray0, gray, thresholdLevel,
                                Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
                                Imgproc.THRESH_BINARY,
                                (rgb.width() + rgb.height()) / 200, t);
                    }

                    Imgproc.findContours(gray, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

                    for (MatOfPoint contour : contours) {
                        MatOfPoint2f temp = new MatOfPoint2f(contour.toArray());

                        double area = Imgproc.contourArea(contour);
                        approxCurve = new MatOfPoint2f();
                        Imgproc.approxPolyDP(temp, approxCurve,
                                Imgproc.arcLength(temp, true) * 0.02, true);

                        if (approxCurve.total() == 4 && area >= maxArea) {
                            double maxCosine = 0;

                            List<Point> curves = approxCurve.toList();
                            for (int j = 2; j < 5; j++) {

                                double cosine = Math.abs(angle(curves.get(j % 4),
                                        curves.get(j - 2), curves.get(j - 1)));
                                maxCosine = Math.max(maxCosine, cosine);
                            }

                            if (maxCosine < 0.3) {
                                maxArea = area;
                                maxId = contours.indexOf(contour);
                            }
                        }
                    }
                }
            }
            Mat srcDraw = srcResized.clone();

            if (maxId >= 0) {
            	int rectangleCount = 0;
            	int lineWidth = 10;
            	int maxDiff = 100;

            	Rect rectCrop = null;
            	Bitmap bitmapCrop = null;

//            	for (int i = 0; i <= maxId; i++) {
            	for (int i = maxId; i >= 0; i--) {
	                Rect rect = Imgproc.boundingRect(contours.get(i));
	                
	                if (rect.height > minHeight && rect.height < maxHeight &&
	                		rect.width > minWidth && rect.width < maxWidth) {
	                	int duplicate = 0;
	                	
	                	for (int j = 0; j < rectangleCount; j++) {
	                		if (rect.x > rectangleList.get(j).getX() - maxDiff &&
	                				rect.x < rectangleList.get(j).getX() + maxDiff &&
	                				rect.y > rectangleList.get(j).getY() - maxDiff &&
	                				rect.y < rectangleList.get(j).getY() + maxDiff) {
	                			duplicate++;
	                		}
	                	}
	                	if (duplicate == 0) {
		                	Imgproc.rectangle(srcDraw, rect.tl(), rect.br(),
		                			new Scalar(255, 0, 0, .8), lineWidth);
		                	
		                	Rectangle tempRectangle = new Rectangle();
		                	
		                	tempRectangle.setId(rectangleCount);
		                	
		                	tempRectangle.setX(rect.x);
		                	tempRectangle.setY(rect.y);
		                	
		                	tempRectangle.setWidth(rect.width);
		                	tempRectangle.setHeight(rect.height);
		                	        
                	        Imgproc.rectangle(srcDraw, new Point(rect.x, rect.y),
                	        		new Point(rect.x + rect.width, rect.y + rect.height),
                	                new Scalar(0, 255, 0));
                	        
                	        if (crop) {
	                	        rectCrop = new Rect(rect.x, rect.y, rect.width, rect.height);
	                	        Mat image_roi = new Mat(srcResized,rectCrop);
                                bitmapCrop = Bitmap.createBitmap(image_roi.cols(), image_roi.rows(), Bitmap.Config.ARGB_8888);
                                Utils.matToBitmap(image_roi, bitmapCrop);
                                tempRectangle.setBitmap(bitmapCrop);
                	        }

                            rectangleList.add(tempRectangle);
			                
//            	        	System.out.println(
//			                		"\n" + rectangleType + " count: " + rectangleList.get(rectangleCount).getId() +
//			                		"\n" + rectangleType + " width: " + rectangleList.get(rectangleCount).getWidth() +
//			                		"\n" + rectangleType + " height: " + rectangleList.get(rectangleCount).getHeight() +
//			                		"\n" + rectangleType + " position: " + rectangleList.get(rectangleCount).getPosition()
//			                		);
			                rectangleCount++;
	                	} else {
//	                		System.out.println("Duplicate rectangle skipped");
	                	}
	                }  // end if
            	}
            	Mat destination = srcDraw.clone();

            	destinationBitmap = Bitmap.createBitmap(destination.cols(), destination.rows(), Bitmap.Config.ARGB_8888);
            	Utils.matToBitmap(destination, destinationBitmap);
//                Imgcodecs.imwrite(testOutputFile, destination);
//                System.out.println("\nImage Processed\n");
            }
        return destinationBitmap;
    }
    private static double angle(Point p1, Point p2, Point p0) {
        double dx1 = p1.x - p0.x;
        double dy1 = p1.y - p0.y;
        double dx2 = p2.x - p0.x;
        double dy2 = p2.y - p0.y;
        return (dx1 * dx2 + dy1 * dy2)
                / Math.sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2)
                + 1e-10);
    }
}
