package com.example.mobile_client;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PictureActivity extends AppCompatActivity {
    private ImageView pictureImageView;
    ProgressDialog progressDialog;
    Bitmap bitmap;
    Bitmap processedBitmap;
    Uri photoURI;
    Map<String, String> newBoardValues;
    String ipAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture);

        pictureImageView = findViewById(R.id.image_view_picture);

        Intent intent = getIntent();
        photoURI = intent.getParcelableExtra("photoURI");

        bitmap = null;
        processedBitmap = null;
        newBoardValues = new HashMap<>();
        try {
            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver() , Uri.parse(photoURI.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        pictureImageView.setImageBitmap(bitmap);

        Button sendData = findViewById(R.id.button_send_data_picture);
        sendData.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(PictureActivity.this);
                        builder.setTitle("Enter IP Address");
                        builder.setMessage("Look at the Desktop Server and enter the IP Address given for connection");

                        final EditText input = new EditText(PictureActivity.this);

                        input.setInputType(InputType.TYPE_CLASS_TEXT);
                        builder.setView(input);

                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ipAddress = input.getText().toString();

                                Thread clientThread = new Client(ipAddress, newBoardValues);
                                clientThread.start();

                                Toast.makeText(PictureActivity.this, "Data has been sent.", Toast.LENGTH_SHORT).show();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        builder.show();
                    }
                }
        );
        progressDialog = new ProgressDialog(PictureActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setTitle("Processing Image");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);
        new Thread(new Runnable() {
            public void run() {
                if (bitmap != null) {
                    newBoardValues = FindRectangles.find(bitmap, processedBitmap, getApplicationContext(), photoURI);
                } else {
                    Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(mainActivityIntent);
                }
                progressDialog.dismiss();
            }
        }).start();
    }
}
