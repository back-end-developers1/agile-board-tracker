package com.example.mobile_client;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.biometrics.BiometricPrompt;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.SuccessContinuation;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;

import java.util.List;
import java.util.concurrent.ExecutionException;

class ScanBarcode {
    static String rawValue;
    static String scan(Bitmap bitmap) {
        // Create FirebaseVisionImage using bitmap
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);

        // Set barcode detector settings
        FirebaseVisionBarcodeDetectorOptions options = new FirebaseVisionBarcodeDetectorOptions.Builder()
                .setBarcodeFormats(FirebaseVisionBarcode.FORMAT_ALL_FORMATS)
                .build();

        // Initialise barcode detector with options
        FirebaseVisionBarcodeDetector detector = FirebaseVision.getInstance().getVisionBarcodeDetector(options);

        Task<List<FirebaseVisionBarcode>> result = detector.detectInImage(image)
                .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionBarcode>>() {
                    @Override
                    public void onSuccess(List<FirebaseVisionBarcode> barcodes) {
                        // Decode the barcode
                        for (FirebaseVisionBarcode barcode : barcodes) {
                            rawValue = barcode.getRawValue();
                        }
                        barcodes = null;
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Task failed with an exception
                        System.out.println("In onFailure");
                        return;
                    }
                });
        try {
            Tasks.await(result);
//            System.out.println("Returning Raw Value: " + rawValue);
            image = null;
            result = null;
            return rawValue;
        } catch (ExecutionException e) {
            System.out.println("Execution Exception");
        } catch (InterruptedException e) {
            System.out.println("Interrupted Exception");
        }
        return "Barcode not found";
    }
}
