package com.example.mobile_client;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import java.io.IOException;

public class ErrorActivity extends AppCompatActivity {
    ImageView errorImageView;
    Uri photoURI;
    Bitmap photoBitmap;
    TextView errorTextView;
    String errorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);

        errorImageView = findViewById(R.id.image_view_error);
        errorTextView = findViewById(R.id.text_view_error);

        Intent intent = getIntent();
        photoURI = intent.getParcelableExtra("photoURI");
        errorMessage = intent.getStringExtra("errorMessage");

        errorTextView.setText(getResources().getString(R.string.error_activity_error) + " " + errorMessage);

        try {
            photoBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver() , Uri.parse(photoURI.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        errorImageView.setImageBitmap(photoBitmap);

        Button mainActivity = findViewById(R.id.button_try_again_error);
        mainActivity.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(mainActivityIntent);
                    }
                }
        );
    }
}
