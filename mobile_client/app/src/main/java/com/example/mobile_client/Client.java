package com.example.mobile_client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class Client extends Thread {

    private String ip = "localhost";
    private Socket socket;
    private PrintWriter out;

    private int port = 10000;

    Map<String,String> map = new HashMap<>();
    JSONObject jdata;

    public Client(Map<String,String> m) {
        setData(m);
    }

    public Client(String host, Map<String,String> m) {
        ip = host;
        setData(m);
    }

    public Client(String host, int p, Map<String,String> m) {
        ip = host;
        port = p;
        setData(m);
    }

    private void setData(Map<String,String> m) {
		jdata = new JSONObject(m);
    }

    public void run() {
        try {
            System.out.println("Attempting to connect...");
            socket = new Socket(ip, port);
            System.out.println("Connected to " + ip + ":"+ port);
            System.out.println("Setting up stream...");
            out = new PrintWriter(socket.getOutputStream(),true);
            System.out.println("Stream set up");
            System.out.println("Sending data: " + jdata.toString());
            out.println(jdata.toString());
            System.out.println("Data sent");

        } catch (IOException e) {
            System.out.println("Connection failed");
        } finally {
            try {
                System.out.println("Closing connections...");
                out.close();
                socket.close();
                System.out.println("Connection closed");
            } catch (IOException e) {
                System.out.println("Closing sockets failed");
            }
        }
    }
}
