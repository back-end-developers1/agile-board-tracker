package com.example.mobile_client;

import android.graphics.Bitmap;

class Rectangle implements Comparable<Rectangle>{
	int id;
	
	Integer x;
	Integer y;
    
    int width;
    int height;

	String fileName;

	Bitmap bitmap;
    String barcodeId;
    
	public Rectangle() {
		this.id = 0;
		this.x = 0;
		this.y = 0;
		this.width = 0;
		this.height = 0;
		this.fileName = "";
		this.bitmap = null;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public String getPosition() {
		return "(" + this.x + "," + this.y + ")";
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

    public String getBarcodeId() {
        return barcodeId;
    }
    public void setBarcodeId(String barcodeId) {
        this.barcodeId = barcodeId;
    }
    public Bitmap getBitmap() {
        return bitmap;
    }
    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
    @Override
    public int compareTo(Rectangle r) {
        return this.getX().compareTo(r.getX());
    }
}
