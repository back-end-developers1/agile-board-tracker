package com.example.mobile_client;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;
import org.opencv.core.Size;

class FindRectangles {
	static Map find(Bitmap bitmap, Bitmap processedBitmap, Context context, Uri photoUri) {
		Intent errorIntent = new Intent(context, ErrorActivity.class);
		errorIntent.putExtra("photoURI", photoUri);
		errorIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		Map<String, String> newBoardValues = new HashMap<>();

		//  crop to whiteboard
		String rectangle = "board";
		ArrayList<Rectangle> board = new ArrayList<Rectangle>();

		int mRatio = 7;  //  the board should take up at least 70% of the image

        int mMaxWidth = 4160; //  13MP camera [ adjust to match android device when updated ]
        int mMaxHeight = 3120;

		int mMinWidth = mMaxWidth / 10 * mRatio;
		int mMinHeight = mMaxHeight / 10 * mRatio;

		System.out.println(rectangle + " mMinWidth: " + mMinWidth);
        System.out.println(rectangle + " mMaxWidth: " + mMaxWidth);
        System.out.println(rectangle + " mMinHeight: " + mMinHeight);
        System.out.println(rectangle + " mMaxHeight: " + mMaxHeight + "\n");

		DetectRectangle.detect(board, bitmap, processedBitmap,
				new Size(mMaxWidth, mMaxHeight), rectangle, true,
				mMinHeight, mMaxHeight, mMinWidth, mMaxWidth);

		System.out.println("Found " + board.size() + " " + "board\n");

		if (board.size() == 1) {
			//  if a single board rectangle is found, find headers
			bitmap = board.get(0).getBitmap();

			rectangle = "header";
			ArrayList<Rectangle> headers = new ArrayList<Rectangle>();

			int nHeaders = 5;   //  number of headers
			int mDiff = 200;
			mRatio = 6;         //  number of headers that can fit in whiteboard vertically

			mMinWidth = (board.get(0).getWidth() / nHeaders) - mDiff;
			mMaxWidth = (board.get(0).getWidth() / nHeaders) + mDiff;
			mMinHeight = (board.get(0).getHeight() / mRatio) - mDiff;
			mMaxHeight = (board.get(0).getHeight() / mRatio) + mDiff;

			System.out.println(rectangle + " mMinWidth: " + mMinWidth);
			System.out.println(rectangle + " mMaxWidth: " + mMaxWidth);
			System.out.println(rectangle + " mMinHeight: " + mMinHeight);
			System.out.println(rectangle + " mMaxHeight: " + mMaxHeight + "\n");

			processedBitmap = null;
			processedBitmap = DetectRectangle.detect(headers, bitmap, processedBitmap,
					new Size(board.get(0).getWidth(), board.get(0).getHeight()),
					rectangle, true, mMinHeight, mMaxHeight, mMinWidth, mMaxWidth);

			if (headers.size() > 0) {
				System.out.println("Found " + headers.size() + " " + "headers\n");
				//  if headers are found, read header barcodes
				int duplicate;
				int barcodesNotFound = 0;
				String barcodeId;
                //  flush barcode scanner
                for (int i = 0; i < 4; i++) {
                    barcodeId = ScanBarcode.scan(headers.get(0).getBitmap());
                }

				for (int i = 0; i < headers.size(); i++) {
					duplicate = 0;
					barcodeId = ScanBarcode.scan(headers.get(i).getBitmap());
					for (int j = 0; j < headers.size() - 1; j++) {
						if (barcodeId == headers.get(j).getBarcodeId()) {
							duplicate++;
						}
					}
					if (duplicate == 0 && !barcodeId.equals("-1")) {
						headers.get(i).setBarcodeId(barcodeId);
//						System.out.println("Header " + headers.get(i).getId() +
//								" contains barcode value " + headers.get(i).getBarcodeId());
					}
					else {
						processedBitmap = null;
						processedBitmap = headers.get(i).getBitmap();
						barcodesNotFound++;
					}
					System.out.println("Header " + headers.get(i).getId() +
							" barcode value " + headers.get(i).getBarcodeId() +
							" has x position " + headers.get(i).getX());
					barcodeId = null;
				}
				if (barcodesNotFound == 0) {
					//  if header barcodes are readable, find tasks
					System.out.println("Header barcodes found");

					bitmap = board.get(0).getBitmap();

					rectangle = "task";
					ArrayList<Rectangle> tasks = new ArrayList<Rectangle>();

					mDiff = 30;
					int mWidthRatio = 18;
					int mHeightRatio = 16;

					mMinWidth = (board.get(0).getWidth() / mWidthRatio) - mDiff;
					mMaxWidth = (board.get(0).getWidth() / mWidthRatio) + mDiff;
					mMinHeight = (board.get(0).getHeight() / mHeightRatio) - mDiff;
					mMaxHeight = (board.get(0).getHeight() / mHeightRatio) + mDiff;

					processedBitmap = null;
					processedBitmap = DetectRectangle.detect(tasks, bitmap, processedBitmap,
							new Size(board.get(0).getWidth(), board.get(0).getHeight()),
							rectangle, true, mMinHeight, mMaxHeight, mMinWidth, mMaxWidth);

					System.out.println("Found " + tasks.size() + " " + "tasks\n");

					if (tasks.size() >= 1) {
						Collections.sort(tasks);    //  sort tasks by X value
                                                    //  this method is implemented in Rectangle.java
						//  if tasks are found, read task barcodes
//						int duplicate = 0;
//						String barcodeId;
						barcodesNotFound = 0;
                        //  flush barcode scanner
                        for (int i = 0; i < 4; i++) {
                            barcodeId = ScanBarcode.scan(headers.get(0).getBitmap());
                        }
						for (int i = 0; i < tasks.size(); i++) {
							duplicate = 0;
							barcodeId = null;
							barcodeId = ScanBarcode.scan(tasks.get(i).getBitmap());
							for (int j = 0; j < tasks.size(); j++) {
								if (barcodeId == tasks.get(j).getBarcodeId()) {
									duplicate++;
								}
							}
							if (duplicate == 0) {
								tasks.get(i).setBarcodeId(barcodeId);
							}
							else {
								processedBitmap = null;
								processedBitmap = tasks.get(i).getBitmap();
								barcodesNotFound++;
							}
							System.out.println("Task " + tasks.get(i).getId() +
									" barcode value " + tasks.get(i).getBarcodeId() +
									" has x position " + tasks.get(i).getX());
						}
						if (barcodesNotFound == 0) {
							//  if task barcodes are readable, create map
							System.out.println("Task barcodes found");

							Collections.sort(headers);  //  sort headers by X value
							                            //  this method is implemented in Rectangle.java

							for (int i = 0; i < tasks.size(); i++) {
								for (int j = 0; j < headers.size(); j++) {
									if (!(j + 1 >= headers.size())) {
										if (tasks.get(i).getX() > headers.get(j).getX() &&
												tasks.get(i).getX() < headers.get(j + 1).getX()) {
											if (!newBoardValues.containsKey(tasks.get(i).getBarcodeId())) {
												System.out.println("Inserting key(task) " + tasks.get(i).getBarcodeId() +
														" with value(header) " + headers.get(j).getBarcodeId());
												newBoardValues.put(tasks.get(i).getBarcodeId(), headers.get(j).getBarcodeId());
											}
										}
									} else {
										if (tasks.get(i).getX() > headers.get(j).getX() &&
												tasks.get(i).getX() < (headers.get(j).getX() + headers.get(j).getWidth())) {
											if (!newBoardValues.containsKey(tasks.get(i).getBarcodeId())) {
												System.out.println("Inserting key(task) " + tasks.get(i).getBarcodeId() +
														" with value(header) " + headers.get(j).getBarcodeId());
												newBoardValues.put(tasks.get(i).getBarcodeId(), headers.get(j).getBarcodeId());
											}
										}
									}
								}
							}
						} else {
							errorIntent.putExtra("errorMessage",barcodesNotFound + " task barcodes could not be read, try again.");
							context.startActivity(errorIntent);
						}
					} else {
						errorIntent.putExtra("errorMessage", "No tasks found, try again.");
						context.startActivity(errorIntent);
					}
				} else {
					errorIntent.putExtra("errorMessage", barcodesNotFound + " header barcodes could not be read, try again.");
					context.startActivity(errorIntent);
				}
			} else {
				errorIntent.putExtra("errorMessage", "No headers found, try again.");
				context.startActivity(errorIntent);
			}
		} else {
			errorIntent.putExtra("errorMessage", "Board not found, try again.");
			context.startActivity(errorIntent);
		}
		return newBoardValues;
	}
}
 	