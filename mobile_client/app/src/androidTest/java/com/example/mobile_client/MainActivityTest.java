package com.example.mobile_client;

import android.app.Activity;
import android.app.Instrumentation;
import android.view.View;

import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertNotNull;

public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    private MainActivity mainActivity = null;

    Instrumentation.ActivityMonitor monitor_picture_activity = getInstrumentation().addMonitor(PictureActivity.class.getName(), null, false);

    @Before
    public void setUp() throws Exception {
        mainActivity = activityTestRule.getActivity();
    }

    //test one -- testing to see if application launches
    @Test
    public void onCreate() {
        View view = mainActivity.findViewById(R.id.button_open_camera);
        assertNotNull(view);
    }

    //test two -- testing to see if 'open camera' button can be clicked
    @Test
    public void testUserClickCameraButton() {
        onView(withId(R.id.button_open_camera)).perform(click());
    }

    //test three -- testing to see if application will shift from main to picture activity
    @Test
    public void testShiftToPictureActivity() {
        //to shift from one activity to another
        assertNotNull(mainActivity.findViewById(R.id.button_open_camera));
        onView(withId(R.id.button_open_camera)).perform(click());

        // *** add camera shutter/click and accept ***

        Activity setDateActivity = getInstrumentation().waitForMonitorWithTimeout(monitor_picture_activity, 50000);

        assertNotNull(setDateActivity);
        setDateActivity.finish();
    }

    @Test
    public void onActivityResult() {
    }

    @After
    public void tearDown() throws Exception {
        mainActivity = null;
    }
}